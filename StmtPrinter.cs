using System;
using System.Linq;
using System.Collections.Generic;
namespace CSLox
{
    public class StmtPrinter : IStatementVisitor<string>
    {
        private ExpPrinter XpPrinter = new ExpPrinter();

        public string Print(Statement statement)
        {
            return statement.Accept(this);
        }

        public string Print(List<Statement> statements)
        {

            return statements.Aggregate("", (acc, s) => acc + s.Accept(this));
        }

        string IStatementVisitor<string>.visitStmtBlock(Statement.Block block)
        {
            string code = block.Statements.Aggregate("", (acc, st) => $"{acc}{Print(st)}\n");
            return $"{{\n{code}\n}}";
        }

        string IStatementVisitor<string>.visitStmtClass(Statement.Class @class)
        {
            throw new NotImplementedException();
        }

        string IStatementVisitor<string>.visitStmtConst(Statement.Const @const)
        {
            return $"const {@const.Name.Lexeme} = {XpPrinter.Print(@const.Value)}";
        }

        string IStatementVisitor<string>.visitStmtExpression(Statement.Expression expression)
        {
            return $"{XpPrinter.Print(expression.Expr)}";
        }

        string IStatementVisitor<string>.visitStmtFunction(Statement.Function function)
        {
            string pars = function.Parameters.Aggregate("", (acc, p) => $"{acc}, {p.Lexeme}");
            return $"<fn {function.Name.Lexeme} ({pars})>\n{Print(function.Body)}";
        }

        string IStatementVisitor<string>.visitStmtIf(Statement.If @if)
        {
            string elseBranch = @if.ElseBranch == null ? "" : $"\nelse\n{Print(@if.ElseBranch)}";
            return $"if ({XpPrinter.Print(@if.Condition)}\n{Print(@if.ThenBranch)})";
        }

        string IStatementVisitor<string>.visitStmtPrint(Statement.Print print)
        {
            return $"print {XpPrinter.Print(print.Expr)}";
        }

        string IStatementVisitor<string>.visitStmtReturn(Statement.Return @return)
        {
            return $"return {this.XpPrinter.Print(@return.Value)}";
        }

        string IStatementVisitor<string>.visitStmtVar(Statement.Var var)
        {
            return $"var {@var.Name.Lexeme} = {XpPrinter.Print(@var.Initializer)}";
        }

        string IStatementVisitor<string>.visitStmtWhile(Statement.While @while)
        {
            return $"while {XpPrinter.Print(@while.Condition)}\n{Print(@while.Body)}";
        }
    }
}