using System;
using System.Linq;
using System.Collections.Generic;

namespace CSLox
{
    public interface ILoxType
    {

    }

    public class LoxClass : ILoxType, ILoxCallable
    {
        private readonly Statement.Class Declaration;
        private readonly Dictionary<string, LoxNamedFunction> Methods;

        public LoxNamedFunction this[string name] => 
            this.Methods.ContainsKey(name) ? this.Methods[name] : null;

        public LoxClass(Statement.Class declaration, Dictionary<string, LoxNamedFunction> methods)
        {
            this.Declaration = declaration;
            this.Methods = methods;
        }

        public string Name => this.Declaration?.Name?.Lexeme ?? "";

        public uint Arity => this.Methods["init"]?.Arity ?? 0u;

        object ILoxCallable.Call(Evaluator evaluator, List<object> arguments)
        {
            var instance = new LoxInstance(this);
            if (this.Methods.ContainsKey("init"))
                this.Methods["init"].bind(instance).Call(evaluator, arguments);
            return instance;
        }
        public override string ToString()
        {
            return this.Name;
        }
    }

    public class LoxInstance 
    {
        private readonly Dictionary<string, object> Fields;
        private readonly LoxClass cls;

        public LoxInstance(LoxClass @class)
        {
            this.cls = @class;
            this.Fields = new Dictionary<string, object>();
        }
        private RuntimeException UndefinedError(Token name) =>
            new RuntimeException(name, $"Undefined member '{name.Lexeme}' at {name.Line}, {name.Column}");
        public object this[Token name] 
        {
            get => Get(name);
            set => Set(name, value);
        }

        private void Set(Token name, object value)
        {
            this.Fields[name.Lexeme] = value;
        }
        private object Get(Token name) 
        {
            if (this.Fields.ContainsKey(name.Lexeme))
                return this.Fields[name.Lexeme];
            LoxNamedFunction method = this.cls[name.Lexeme];
            if (method != null) return method.bind(this);
            throw UndefinedError(name);
        }

        public override string ToString()
        {
            return $"{cls} instance";
        }
    }
}