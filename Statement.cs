using System.Collections.Generic;

namespace CSLox
{
	public interface IStatementVisitor<T>
	{
		T visitStmtBlock(Statement.Block block);
		T visitStmtExpression(Statement.Expression expression);
		T visitStmtPrint(Statement.Print print);
		T visitStmtWhile(Statement.While @while);
		T visitStmtClass(Statement.Class @class);
		T visitStmtFunction(Statement.Function function);
		T visitStmtIf(Statement.If @if);
		T visitStmtVar(Statement.Var var);
		T visitStmtConst(Statement.Const @const);
		T visitStmtReturn(Statement.Return @return);

	}

	public abstract class Statement
	{
		public abstract T Accept<T>(IStatementVisitor<T> visitor);

		public sealed class Block : Statement 
		{
			public List<Statement> Statements;

			public Block(List<Statement> statements)
			{
				this.Statements = statements;

			}
 
			public override T Accept<T>(IStatementVisitor<T> visitor)
			{
				return visitor.visitStmtBlock(this);
			}

		}


		public sealed class Expression : Statement 
		{
			public CSLox.Expression Expr;

			public Expression(CSLox.Expression expr)
			{
				this.Expr = expr;

			}

			public override T Accept<T>(IStatementVisitor<T> visitor)
			{
				return visitor.visitStmtExpression(this);
			}

		}


		public sealed class Print : Statement 
		{
			public CSLox.Expression Expr;

			public Print(CSLox.Expression expr)
			{
				this.Expr = expr;

			}

			public override T Accept<T>(IStatementVisitor<T> visitor)
			{
				return visitor.visitStmtPrint(this);
			}

		}


		public sealed class While : Statement 
		{
			public CSLox.Expression Condition;
			public Statement Body;

			public While(CSLox.Expression condition, Statement body)
			{
				this.Condition = condition;
				this.Body = body;

			}

			public override T Accept<T>(IStatementVisitor<T> visitor)
			{
				return visitor.visitStmtWhile(this);
			}

		}


		public sealed class Class : Statement 
		{
			public Token Name;
			public List<Statement.Function> Methods;

			public Class(Token name, List<Statement.Function> methods)
			{
				this.Name = name;
				this.Methods = methods;

			}

			public override T Accept<T>(IStatementVisitor<T> visitor)
			{
				return visitor.visitStmtClass(this);
			}

		}


		public sealed class Function : Statement 
		{
			public Token Name;
			public List<Token> Parameters;
			public List<Statement> Body;

			public Function(Token name, List<Token> parameters, List<Statement> body)
			{
				this.Name = name;
				this.Parameters = parameters;
				this.Body = body;

			}

			public override T Accept<T>(IStatementVisitor<T> visitor)
			{
				return visitor.visitStmtFunction(this);
			}

		}


		public sealed class If : Statement 
		{
			public CSLox.Expression Condition;
			public Statement ThenBranch;
			public Statement ElseBranch;

			public If(CSLox.Expression condition, Statement thenbranch, Statement elsebranch)
			{
				this.Condition = condition;
				this.ThenBranch = thenbranch;
				this.ElseBranch = elsebranch;

			}

			public override T Accept<T>(IStatementVisitor<T> visitor)
			{
				return visitor.visitStmtIf(this);
			}

		}


		public sealed class Var : Statement 
		{
			public Token Name;
			public CSLox.Expression Initializer;

			public Var(Token name, CSLox.Expression initializer)
			{
				this.Name = name;
				this.Initializer = initializer;

			}

			public override T Accept<T>(IStatementVisitor<T> visitor)
			{
				return visitor.visitStmtVar(this);
			}

		}


		public sealed class Const : Statement 
		{
			public Token Name;
			public CSLox.Expression Value;

			public Const(Token name, CSLox.Expression value)
			{
				this.Name = name;
				this.Value = value;

			}

			public override T Accept<T>(IStatementVisitor<T> visitor)
			{
				return visitor.visitStmtConst(this);
			}

		}


		public sealed class Return : Statement 
		{
			public Token Keyword;
			public CSLox.Expression Value;

			public Return(Token keyword, CSLox.Expression value)
			{
				this.Keyword = keyword;
				this.Value = value;

			}

			public override T Accept<T>(IStatementVisitor<T> visitor)
			{
				return visitor.visitStmtReturn(this);
			}

		}

	}
}
