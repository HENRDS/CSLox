using System;
using System.Linq;
using System.Collections.Generic;

namespace CSLox
{
    public interface ILoxCallable
    {

        uint Arity { get; }

        object Call(Evaluator evaluator, List<object> arguments);
    }

    public class LoxNative : ILoxCallable
    {
        
        Func<Evaluator, List<object>, object> RealFunction;
        uint arity;
        public LoxNative(uint arity, Func<Evaluator, List<object>, object> realFunction)
        {
            this.arity = arity;
            this.RealFunction = realFunction;
        }

        public uint Arity => arity;

        public object Call(Evaluator evaluator, List<object> arguments) 
            => RealFunction(evaluator, arguments); 
    }

    

    public class LoxFunction : ILoxCallable
    {
        protected Environment Closure;
        protected List<Token> Parameters;
        protected List<Statement> Body;
        protected bool IsInitializer;

        
        public LoxFunction(List<Token> parameters, List<Statement> body, Environment closure, bool isInitializer = false)
        {
            this.Parameters = parameters;
            this.Body = body;
            this.Closure = closure;
            this.IsInitializer = isInitializer;
        }

        public uint Arity => (uint)(Parameters?.Count ?? 0);

        public object Call(Evaluator evaluator, List<object> arguments)
        {
            var environment = new Environment(this.Closure);
            var parameters = Parameters.Zip(arguments, (p, a) => new {Par = p, Arg = a});
            foreach (var p in parameters)
                environment.Define(p.Par, p.Arg);
            try
            {
                evaluator.ExecuteBlock(Body, environment);
            }
            catch (ReturnException r)
            {
                return r.Value;
            }
            if (this.IsInitializer) return this.Closure["this", 0];
            return null;
        }
    }


    public class LoxNamedFunction : LoxFunction 
    {
        Statement.Function Declaration;
        public string Name => this.Declaration?.Name?.Lexeme ?? "";
        public LoxNamedFunction(Statement.Function declaration, Environment closure, bool isInitializer) : 
            base(declaration.Parameters, declaration.Body, closure, isInitializer)
        {
            this.Declaration = declaration;            
        }

        public LoxNamedFunction bind(LoxInstance instance)
        {
            var environment = new Environment(this.Closure);
            environment.DefineSafe("this", instance);
            return new LoxNamedFunction(this.Declaration, environment, this.IsInitializer);
        }

        public override string ToString()
        {
            return $"<fn {Declaration.Name.Lexeme}>";
        }
    }

    public class LoxLambda : LoxFunction
    {
        Expression.Lambda Lambda;
        public LoxLambda(Expression.Lambda lambda, Environment closure):
            base(lambda.Parameters, lambda.Body, closure)
        {
            this.Lambda = lambda;
        }
        
        public override string ToString()
        {
            string pars = this.Parameters.Aggregate("", (acc, p) => $"{acc} {p.Lexeme}");
            return $"<λ{pars} >";
        }

    }


}