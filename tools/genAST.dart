import "dart:io";
import "dart:convert";
enum IndentationLevel {
  namespace, topLvl, subclass, member, memberbody
}

String indent(IndentationLevel level) => "\t" * level.index;

class Property {
  
  final String name;
  final String type;
  final String argName;
  Property(String type, String name) :
    name = name,
    type = type,
    argName = checkKeywd(name.toLowerCase());
}

class ClassInfo { 
  final String name;
  final String baseAbbr;
  final String baseClass;
  final String visitorName;
  String argName;
  List<Property> properties;

  ClassInfo(String name, String baseAbbr, String baseClass, String visitor) 
    : name = name, baseAbbr = baseAbbr, baseClass = baseClass, visitorName = visitor 
    {
      this.argName = checkKeywd(name.toLowerCase());
      this.properties= new List<Property>();
    } 
  String get visitMethod => "visit${baseAbbr}$name";

  void addProp(Property p) {
    this.properties.add(p);
  }
  
  String joinProps() =>
    properties.fold("", (acc, p) => "$acc${indent(IndentationLevel.member)}public ${p.type} ${p.name};\n");

  String joinArgs() =>
    properties.map((p) => "${p.type} ${p.argName}").join(", ");

  String constructor() {
    var buffer = new StringBuffer();
    String args = joinArgs();
    String declInd = indent(IndentationLevel.member);
    String bodyInd = indent(IndentationLevel.memberbody);
    var body = properties.fold("", (acc, p) => "$acc${bodyInd}this.${p.name} = ${p.argName};\n");

    buffer.writeln("${declInd}public $name($args)");    
    buffer.writeln("${declInd}{");    
    buffer.writeln(body);    
    buffer.writeln("${declInd}}"); 
    return buffer.toString();   
  }

  String extSuper() {
    //  not the most generic code but what the hell
    var buffer = new StringBuffer();
    String memInd = indent(IndentationLevel.member);
    String bodyInd = indent(IndentationLevel.memberbody);
    buffer.writeln("${memInd}public override T Accept<T>($visitorName<T> visitor)");
    buffer.writeln("${memInd}{");
    buffer.writeln("${bodyInd}return visitor.$visitMethod(this);");
    buffer.writeln("${memInd}}");
    return buffer.toString();
  }

  String define() {
    String props = joinProps();
    var buffer = new StringBuffer();
    var clsInd = indent(IndentationLevel.subclass);
    buffer.writeln("");
    buffer.writeln("${clsInd}public sealed class $name : $baseClass ");
    buffer.writeln("${clsInd}{");
    buffer.writeln(props);
    buffer.writeln(constructor());
    buffer.writeln(extSuper());
    buffer.writeln("${clsInd}}");
    return buffer.toString();
  }

}

List<String> CSKeywords = 
  ['abstract', 'as', 'base', 'bool', 'break', 'byte', 'case', 'catch', 'char', 'checked', 'class', 'const', 'continue', 
 'decimal', 'default', 'delegate', 'do', 'double', 'else', 'enum', 'event', 'explicit', 'extern', 'false', 'finally', 
 'fixed', 'float', 'for', 'foreach', 'goto', 'if', 'implicit', 'in', 'in', 'int', 'interface', 'internal', 'is', 
 'lock', 'long', 'namespace', 'new', 'null', 'object', 'operator', 'out', 'out', '(generic', 'modifier)', 'override', 
 'params', 'private', 'protected', 'public', 'readonly', 'ref', 'return', 'sbyte', 'sealed', 'short', 'sizeof', 
 'stackalloc', 'static', 'string', 'struct', 'switch', 'this', 'throw', 'true', 'try', 'typeof', 'uint', 'ulong', 
 'unchecked', 'unsafe', 'ushort', 'using', 'using', 'static', 'virtual', 'void', 'volatile', 'while'];

 

String checkKeywd(String s) {
  if (CSKeywords.contains(s))
      s = "@$s";
      return s;
}

ClassInfo defineType(String name, String baseAbbr, String baseName, String visitor,String fields) {
  var clsInfo = new ClassInfo(name, baseAbbr, baseName, visitor);
  var regx = new RegExp(r"([\w\.<>]+)\s+(\w+)");
  stdout.writeln(name);
  for (Match m in regx.allMatches(fields)) {
    stdout.writeln("\t${m.group(1)} ${m.group(2)}");
    clsInfo.addProp(new Property(m.group(1), m.group(2)));
  }
  return clsInfo;
}


String defineVisitor(String name, List<String> lines) {
  var buffer = new StringBuffer();
  var ind = indent(IndentationLevel.topLvl);
  var body = lines.fold("", (acc,l) => "$acc${indent(IndentationLevel.subclass)}$l\n"); 
  buffer.writeln("${ind}public interface $name<T>");
  buffer.writeln("${ind}{");
  buffer.writeln(body);
  buffer.writeln("${ind}}");
  return buffer.toString();
}


void defineAST(String name, String abbr,String path) {
  var file = new File(path)
    .openRead()
    .transform(UTF8.decoder)
    .transform(const LineSplitter());

  var output = new File("$name.cs").openWrite();
  var visitorName = "I${name}Visitor";
  var ind = indent(IndentationLevel.topLvl);
  var sind = indent(IndentationLevel.subclass);

  output.writeln("using System.Collections.Generic;");
  output.writeln("");
  output.writeln("namespace CSLox");
  output.writeln("{");

  var classes = new List<String>();
  var visits = new List<String>();

  var whenDone = () {
    output.writeln(defineVisitor(visitorName, visits));
    output.writeln("${ind}public abstract class $name");
    output.writeln("${ind}{");
    output.writeln("${sind}public abstract T Accept<T>($visitorName<T> visitor);");
    output.writeln("${classes.join("\n")}");
    output.writeln("${ind}}");
    output.writeln("}");
  };
  

  file.listen((String line) {
    var pieces = line.split(":");
    var classname = pieces[0].trim();
    var cls = defineType(classname, abbr, name, visitorName, pieces[1]);
    classes.add(cls.define());
    visits.add("T ${cls.visitMethod}($name.${cls.name} ${cls.argName});");
  }, onError: (e) => print(e.toString())
  , onDone: whenDone );  
  


}

void main (List<String> argv) {
  defineAST("Expression", "Expr", "structure");
  defineAST("Statement", "Stmt", "stmts");
}