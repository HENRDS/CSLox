using System;
using System.Text;
using System.Linq;

namespace CSLox
{
    public class ExpPrinter : IExpressionVisitor<string>
    {
        public String Print(Expression expr)
        {
            return expr.Accept<String>(this);
        }

        private string Parenthesize(string name, params Expression[] exprs) 
        {
            var builder = new StringBuilder();
            builder.Append($"({name}");
            foreach (var x in exprs)
                builder.Append(" ").Append(x.Accept(this));
            builder.Append(")");
            return builder.ToString();
        }

        string IExpressionVisitor<string>.visitExprAssignment(Expression.Assignment assignment)
        {
            return $"{assignment.Name} <- {assignment.Value.Accept(this)}";
        }

        string IExpressionVisitor<string>.visitExprGrouping(Expression.Grouping grouping)
        {
            return Parenthesize("grouping", grouping.Expr);
        }

        string IExpressionVisitor<string>.visitExprConditional(Expression.Conditional conditional)
        {
            return null;
            // return Parenthesize($"{ternary.FirstOp.Lexeme}{ternary.SecondOp.Lexeme}", ternary.Left, ternary.Middle, ternary.Right);
        }

        string IExpressionVisitor<string>.visitExprBinary(Expression.Binary binary)
        {
            return Parenthesize(binary.Operator.Lexeme, binary.Left, binary.Right);
        }

        string IExpressionVisitor<string>.visitExprUnary(Expression.Unary unary)
        {
            return Parenthesize(unary.Operator.Lexeme, unary.Right);
        }

        string IExpressionVisitor<string>.visitExprVariable(Expression.Variable variable)
        {
            return variable.Name.Lexeme;
        }

        string IExpressionVisitor<string>.visitExprLiteral(Expression.Literal literal)
        {
            return literal.Value?.ToString() ?? "nil";
        }

        string IExpressionVisitor<string>.visitExprLogic(Expression.Logic logic)
        {
            return Parenthesize(logic.Operator.Lexeme, logic.Left, logic.Right);
        }

        string IExpressionVisitor<string>.visitExprComma(Expression.Comma comma)
        {
            Func<string, string> fn = (x) => x == "" ? "" : ",";
            return comma.Expressions.Select(x => Print(x)).Aggregate("", (acc, s) => $"{fn(acc)} {s}");
        }

        string IExpressionVisitor<string>.visitExprCall(Expression.Call call)
        {
            Func<string, string> fn = (x) => x == "" ? "" : ",";
            string args = call.Arguments.Aggregate("", (acc, s) => $"{fn(acc)} {Print(s)}");
            return $"{Print(call.Callee)}({args})";
        }

        string IExpressionVisitor<string>.visitExprLambda(Expression.Lambda lambda)
        {
            throw new NotImplementedException();
        }

        string IExpressionVisitor<string>.visitExprGet(Expression.Get get)
        {
            throw new NotImplementedException();
        }

        string IExpressionVisitor<string>.visitExprSet(Expression.Set set)
        {
            throw new NotImplementedException();
        }
    }
}