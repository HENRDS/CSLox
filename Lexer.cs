using System;
using System.IO;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using CSLox.Lexing;
using System.Text.RegularExpressions;

namespace CSLox
{
    public class Lexer : IDisposable
    {
        public static readonly string[] keywords = {"class", "else", "false", 
            "fun", "for", "if", "nil", "print", "return", "super", "this", 
            "true", "var", "while", "const"};
        private SourcePosition LastLexemePos;
        private static readonly Char[] Powers = {'⁰', '¹', '²', '³', '⁴', '⁵', '⁶', '⁷', '⁸', '⁹', '⁻'}; 
        private readonly SourceReader Source ;
        public List<Token> Tokens = new List<Token>();

        public Lexer(string path) 
        {
            // this.Source = source;
            this.Source = new SourceReader(path);
        }

        private string Cap()
        {
            return this.Source.Capture(ref this.LastLexemePos);
        }
        private void AddToken(TokenType type, string lexeme = null, object literal = null)
        {
            string text = lexeme ?? this.Cap();
            Tokens.Add(new Token(type, text, literal, this.LastLexemePos.Line, this.LastLexemePos.Column));
        }

        private void Error(string message) 
        {
            Lox.Handler.Error<Lexer>(message, this.Source.Line, this.Source.Column);
        }

        private void Identifier()
        {
            while (CharactersHelper.IsIdentifierChar(this.Source.Peek())) 
                this.Source.Next();
            string lit = this.Cap();
            if (keywords.Contains(lit))
            {
                AddToken(Enum.Parse<TokenType>(lit, true), lit);
                return;
            }
            if (lit == "_") AddToken(TokenType.WildCard, lit);
            else AddToken(TokenType.Identifier, lit);
        }

        private void ScanDecimals()
        {
            char c = this.Source.Peek();
            while (CharactersHelper.IsDecimalDig(c))
            {
                this.Source.Next();
                c = this.Source.Peek();
            }
        }

        private void NumLiteral() 
        {    
            string sl = null;
            ScanDecimals();
            if (this.Source.Match('.'))
            {
                ScanDecimals();
                sl = this.Cap();
                double dlit;
                if (Double.TryParse(sl, out dlit))
                    AddToken(TokenType.Num, sl, dlit);
                else
                    Error($"Invalid Num literal '{sl}'");
                return;
            }
            sl = this.Cap();
            int ilit;
            if (int.TryParse(sl, out ilit))
                AddToken(TokenType.Int, sl, ilit);
            else 
                Error($"Invalid int literal '{sl}'");

            // string lit = MatchRegex(NumRegex);
            // if (lit != null)
                // AddToken(TokenType.NUMBER, Double.Parse(lit));
        }

        private char ParseUnicodeEscape()
        {
            return '\0';
        }

        private char EscapedChar()
        {
            switch(this.Source.Next())
            {
                case 'n': return '\n';
                case 'r': return '\r';
                case 'f': return '\f';
                case 'b': return '\b';
                case '"': return '\"';
                case 'u': return ParseUnicodeEscape();
                default: 
                    Error("Unknown escape sequence"); 
                    break;
            }
            return SourceReader.InvalidChar;
        }
        private void StrLiteral(bool isRaw = false) 
        {
            var builder = new StringBuilder();
            while (!this.Source.IsAtEnd)
            {
                char c = this.Source.Next();
                switch(c)
                {
                    case '\\':
                        if (isRaw) goto default;
                        builder.Append(EscapedChar());
                        break;
                    case '"':
                        AddToken(TokenType.Str, literal:builder.ToString());
                        return;
                    default:
                        if (isRaw || !CharactersHelper.IsLineTerminator(c))
                            builder.Append(c);
                        else
                            Error($"'{c}' Only raw strings can be multiline. Use \"\"\" ... \"\"\".");
                        break;
                }
            } 
            Error("Unterminated string.");            
        }

        private void MultilineComment()
        {
            char c = this.Source.Peek();
            while(!this.Source.IsAtEnd)
            {
                if (c == SourceReader.InvalidChar) break;
                if (c == '*')
                {
                    if (this.Source.Match('/'))
                        return;
                }
                c = this.Source.Next();
            }
            Error("Unterminated multiline comment.");
            // var m = CommentRegex.Match(Source, Start);
            // // yo, shit's lit
            // string lit = null;
            // if (m.Success && m.Index == Start)
            // {
            //     lit = Source.Substring(Start, m.Length);
            //     Console.WriteLine(lit);
            //     Current += m.Length - 1;
            //     this.Line += lit.Aggregate(0, (acc, c) => c == '\n' ? acc+1 : acc);
            // }
            // else
            // {
            // }   

        }
        private void NextToken() 
        {
            char c = this.Source.Next();
            switch(c) 
            {
                case '(': AddToken(TokenType.LParen); break;
                case ')': AddToken(TokenType.RParen); break;
                case '{': AddToken(TokenType.LBrace); break;
                case '}': AddToken(TokenType.RBrace); break;
                case ',': AddToken(TokenType.Comma); break;
                case '.': AddToken(TokenType.Dot); break;
                case '-': 
                    if (this.Source.Match('>')) AddToken(TokenType.MinusGreater);
                    else if (this.Source.Match('-')) AddToken(TokenType.MinusMinus);
                    else AddToken(TokenType.Minus); 
                    break;
                case '+': AddToken(this.Source.Match('+') ? TokenType.PlusPlus : TokenType.Plus); break;
                case ';': AddToken(TokenType.Semicolon); break;
                case '*': AddToken(this.Source.Match('*') ? TokenType.StarStar : TokenType.Star); break;
                case '&': AddToken(this.Source.Match('&') ? TokenType.AmpersandAmpersand : TokenType.Ampersand); break;
                case '|': AddToken(this.Source.Match('|') ? TokenType.PipePipe : TokenType.Pipe); break;
                case '^': AddToken(TokenType.Caret); break;
                case '~': AddToken(TokenType.Tilde); break;
                case '?': AddToken(TokenType.Question); break;
                case '\\': AddToken(TokenType.Backslash); break; 
                case ':': AddToken(TokenType.Colon); break;
                case '!': AddToken(this.Source.Match('=') ? TokenType.BangEquals : TokenType.Bang); break;
                case '=': AddToken(this.Source.Match('=') ? TokenType.EqualsEquals : TokenType.Equals); break;
                case '>': AddToken(this.Source.Match('=') ? TokenType.GreaterEquals : TokenType.Greater); break;
                case '<': AddToken(this.Source.Match('=') ? TokenType.LessEquals : TokenType.Less); break;
                case '/':
                    if (this.Source.Match('/'))
                        LineComment();
                    else if (this.Source.Match('*')) 
                        MultilineComment();
                    else 
                        AddToken(TokenType.Slash); 
                    break;
                case '\r': 
                    if (this.Source.Match('\n')) this.Source.NewLine();
                    break;
                case ' ': 
                case '\t':
                    break;
                case '\u0085':
                case '\u2028':
                case '\u2029':
                case '\n': 
                    this.Source.NewLine(); 
                    break;
                case '"' : 
                    StrLiteral(); 
                    break;
                default:
                    if (CharactersHelper.IsDecimalDig(c)) NumLiteral();
                    else if (CharactersHelper.IsIdentifierFirstChar(c)) {
                        Identifier();
                    }
                    // else if (Powers.Contains(c)) {
                    //     AddToken(TokenType.POW);
                        
                    // }
                    else Error($"Unexpected character: {c}");
                    break;
            }
        }
        private void LineComment()
        {
            while (!CharactersHelper.IsLineTerminator(this.Source.Peek()))
                this.Source.Next();           
        }
        public void Lex()
        {
            while (!this.Source.IsAtEnd)
            {
                this.Source.Sync();
                NextToken();
            }
            this.AddToken(TokenType.Eof);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }
                this.Source.Close();
                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~Lexer() 
        {
          // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
          Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}