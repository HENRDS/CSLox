using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;


namespace CSLox
{
    public class Environment
    {
        readonly Environment Enclosing;
        public static bool IsREPL = false;
        public sealed class Constant
        {
            private readonly object value;
            public object Value => value;
            public Constant(object value) { this.value = value; }
        }

        private Dictionary<string, object> Values;
        public readonly int ID;

        public int NewSub() => this.subs++;
        private int subs = 0;

        public string Path()
        {
            if(this.Enclosing == null) return this.ID.ToString();
            return $"{this.Enclosing.Path()}.{this.ID}";
        }
        private RuntimeException Error(string message, Token tk)
        {
            Lox.Handler.Error<Environment>(message, tk);
            throw new RuntimeException(tk, message);
        }
        private static int cnt = 0;
        public Environment()
        {
            this.ID = cnt++;
            this.Values = new Dictionary<string, object>();
            this.Enclosing = null;
        }
        public Environment(Environment enclosing)
        {
            this.Values = new Dictionary<string, object>();
            this.Enclosing = enclosing;
            this.ID = Enclosing?.NewSub() ?? cnt++;
        }

        public object this[string name, int depth] => EnvAt(depth).Values[name];

        public object this[Token name, int distance] 
        {
            get => EnvAt(distance).Values[name.Lexeme];
            set => EnvAt(distance).Values[name.Lexeme] = value;
        } 
        public object this[Token name] 
        {
            get => Get(name);
            set => Assign(name, value);
        }

        public static Environment operator--(Environment environment) => environment.Enclosing ?? environment;

        private Environment EnvAt(int distance) 
        {
            var env = this;
            while (distance-- > 0) env--;
            return env;
        }

        private void CheckExistence(Token tk)
        {
            if (IsREPL) return;
            string name = tk.Lexeme;
            if (Values.ContainsKey(name))
                throw Error($"{Values[name].GetType().Name} '{name}' already exists", tk);
        }



        public void DefineSafe(string name, object value) => Values[name] = value;
        public void Define(Token token, object value) 
        {
            // Console.WriteLine($"defined {token}");
            CheckExistence(token);
            Values[token.Lexeme] = value;
        }

        public void DefineConst(Token token, object value) 
        {
            CheckExistence(token);
            Values[token.Lexeme] = new Constant(value);
        }

        private object Unwrap(object value)
        {
            if (value is Constant)
                return ((Constant)value).Value;
            else 
                return value;
        }
        private object Get(Token token)
        {
            if (token.Lexeme == "x") 
            {
                Console.WriteLine("resolving X");
                Console.WriteLine(this);
                Console.WriteLine("end");
            }
            string name = token.Lexeme;
            if (Values.ContainsKey(name)) {
                var v = Unwrap(Values[name]);
                if (v == null) throw Error("Use of unitialized variable.", token);
                return v;
            }
            if (this.Enclosing != null) return this.Enclosing.Get(token);
            throw Error($"Undefined reference to '{name}'.", token);
        }
        private void Assign(Token token, object value)
        {
            string name = token.Lexeme;
            if (Values.ContainsKey(name))
            {
                if (value is Constant) 
                    throw Error($"Trying to assign value to the constant '{name}'.", token);
                Values[name] = value;
                return;
            }
            if (Enclosing != null)
            {
                Enclosing.Assign(token, value);
                return;
            } 
            throw Error($"Undefined identifier '{name}'.", token);
        }
        public override string ToString()
        {
            var builder = new StringBuilder();
            foreach (var (n, v) in this.Values)
            {
                string s;
                switch (v)
                {
                    case Constant c:
                        s = $"Const {c.Value.ToString()}";
                        break;
                    default:
                        s = v.ToString();
                        break;
                }
                builder.AppendLine($"{n:40}|{s}");
            }
            if (this.Enclosing != null)
            {
                builder.AppendLine("Enclosed by:");
                builder.AppendLine(this.Enclosing.ToString());
            }
            return builder.ToString();
        }
    }
}