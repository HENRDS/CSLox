﻿using System;
using System.IO;
using System.Linq;
using System.Diagnostics;
namespace CSLox
{
    public class Program
    {
        private static readonly Lox Interpreter = new Lox();
        public static void About()
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            var version = System.Reflection.AssemblyName.GetAssemblyName(assembly.Location).Version.ToString();

            Console.WriteLine($"CSLox version {version}");
            Console.WriteLine("C# implementation of the lox interpreter described in ");
            Console.WriteLine("\"Hand-crafted by Robert Nystrom — © 2015 – 2017\"");
        }
        public static void Interactive() 
        {
            Environment.IsREPL = true;
            About();
            while(true) 
            {
                Lox.Handler.HadError = false;
                Console.Write(">");
                string line = Console.ReadLine();
                Interpreter.Interpret(line);    
            }
        }
        public static void Usage() 
        {
            About();
            Console.WriteLine("Usage: CSLox [script]");
        }
        public static void Main(string[] args)
        {
            switch (args.Length) {
                case 0: 
                    Interactive();
                    break;
                case 1:
                    var clock = Stopwatch.StartNew();
                    Interpreter.Interpret(args[0]);
                    clock.Stop();
                    Console.WriteLine($"Full execution took {clock.ElapsedMilliseconds} ms.");
                    break;
                default: 
                    Usage();
                    break;
            }
        }
    }
}
