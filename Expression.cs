using System.Collections.Generic;

namespace CSLox
{
	public interface IExpressionVisitor<T>
	{
		T visitExprAssignment(Expression.Assignment assignment);
		T visitExprComma(Expression.Comma comma);
		T visitExprConditional(Expression.Conditional conditional);
		T visitExprLogic(Expression.Logic logic);
		T visitExprBinary(Expression.Binary binary);
		T visitExprUnary(Expression.Unary unary);
		T visitExprCall(Expression.Call call);
		T visitExprGet(Expression.Get get);
		T visitExprSet(Expression.Set set);
		T visitExprThis(Expression.This @this);
		T visitExprLambda(Expression.Lambda lambda);
		T visitExprGrouping(Expression.Grouping grouping);
		T visitExprVariable(Expression.Variable variable);
		T visitExprLiteral(Expression.Literal literal);

	}

	public abstract class Expression
	{
		public abstract T Accept<T>(IExpressionVisitor<T> visitor);

		public sealed class Assignment : Expression 
		{
			public Token Name;
			public Expression Value;

			public Assignment(Token name, Expression value)
			{
				this.Name = name;
				this.Value = value;

			}

			public override T Accept<T>(IExpressionVisitor<T> visitor)
			{
				return visitor.visitExprAssignment(this);
			}

		}


		public sealed class Comma : Expression 
		{
			public List<Expression> Expressions;

			public Comma(List<Expression> expressions)
			{
				this.Expressions = expressions;

			}

			public override T Accept<T>(IExpressionVisitor<T> visitor)
			{
				return visitor.visitExprComma(this);
			}

		}


		public sealed class Conditional : Expression 
		{
			public Expression Condition;
			public Expression ThenExpr;
			public Expression ElseExpr;

			public Conditional(Expression condition, Expression thenexpr, Expression elseexpr)
			{
				this.Condition = condition;
				this.ThenExpr = thenexpr;
				this.ElseExpr = elseexpr;

			}

			public override T Accept<T>(IExpressionVisitor<T> visitor)
			{
				return visitor.visitExprConditional(this);
			}

		}


		public sealed class Logic : Expression 
		{
			public Expression Left;
			public Token Operator;
			public Expression Right;

			public Logic(Expression left, Token @operator, Expression right)
			{
				this.Left = left;
				this.Operator = @operator;
				this.Right = right;

			}

			public override T Accept<T>(IExpressionVisitor<T> visitor)
			{
				return visitor.visitExprLogic(this);
			}

		}


		public sealed class Binary : Expression 
		{
			public Expression Left;
			public Token Operator;
			public Expression Right;

			public Binary(Expression left, Token @operator, Expression right)
			{
				this.Left = left;
				this.Operator = @operator;
				this.Right = right;

			}

			public override T Accept<T>(IExpressionVisitor<T> visitor)
			{
				return visitor.visitExprBinary(this);
			}

		}


		public sealed class Unary : Expression 
		{
			public Token Operator;
			public Expression Right;

			public Unary(Token @operator, Expression right)
			{
				this.Operator = @operator;
				this.Right = right;

			}

			public override T Accept<T>(IExpressionVisitor<T> visitor)
			{
				return visitor.visitExprUnary(this);
			}

		}


		public sealed class Call : Expression 
		{
			public Expression Callee;
			public Token Paren;
			public List<Expression> Arguments;

			public Call(Expression callee, Token paren, List<Expression> arguments)
			{
				this.Callee = callee;
				this.Paren = paren;
				this.Arguments = arguments;

			}

			public override T Accept<T>(IExpressionVisitor<T> visitor)
			{
				return visitor.visitExprCall(this);
			}

		}


		public sealed class Get : Expression 
		{
			public Expression Object;
			public Token Name;

			public Get(Expression @object, Token name)
			{
				this.Object = @object;
				this.Name = name;

			}

			public override T Accept<T>(IExpressionVisitor<T> visitor)
			{
				return visitor.visitExprGet(this);
			}

		}


		public sealed class Set : Expression 
		{
			public Expression Object;
			public Token Name;
			public Expression Value;

			public Set(Expression @object, Token name, Expression value)
			{
				this.Object = @object;
				this.Name = name;
				this.Value = value;

			}

			public override T Accept<T>(IExpressionVisitor<T> visitor)
			{
				return visitor.visitExprSet(this);
			}

		}


		public sealed class This : Expression 
		{
			public Token Keyword;

			public This(Token keyword)
			{
				this.Keyword = keyword;

			}

			public override T Accept<T>(IExpressionVisitor<T> visitor)
			{
				return visitor.visitExprThis(this);
			}

		}


		public sealed class Lambda : Expression 
		{
			public Token Keyword;
			public List<Token> Parameters;
			public List<Statement> Body;

			public Lambda(Token keyword, List<Token> parameters, List<Statement> body)
			{
				this.Keyword = keyword;
				this.Parameters = parameters;
				this.Body = body;

			}

			public override T Accept<T>(IExpressionVisitor<T> visitor)
			{
				return visitor.visitExprLambda(this);
			}

		}


		public sealed class Grouping : Expression 
		{
			public Expression Expr;

			public Grouping(Expression expr)
			{
				this.Expr = expr;

			}

			public override T Accept<T>(IExpressionVisitor<T> visitor)
			{
				return visitor.visitExprGrouping(this);
			}

		}


		public sealed class Variable : Expression 
		{
			public Token Name;

			public Variable(Token name)
			{
				this.Name = name;

			}

			public override T Accept<T>(IExpressionVisitor<T> visitor)
			{
				return visitor.visitExprVariable(this);
			}

		}


		public sealed class Literal : Expression 
		{
			public object Value;

			public Literal(object value)
			{
				this.Value = value;

			}

			public override T Accept<T>(IExpressionVisitor<T> visitor)
			{
				return visitor.visitExprLiteral(this);
			}

		}

	}
}
