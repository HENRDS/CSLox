using System;
using System.IO;
using System.Linq;
using System.Collections.Generic; 
using System.Diagnostics; 

namespace CSLox 
{
    
    public class Lox 
    {
        public static ErrorHandler Handler => CSLox.ErrorHandler.Instance;
        private readonly Evaluator evaluator = new Evaluator();


        public void Interpret(string code)
        {
            List<Token> tokens;
            // var watch = Stopwatch.StartNew();
            using (var lexer = new Lexer(code))
            {
                lexer.Lex();
                // Console.WriteLine($"Lexing took {watch.ElapsedMilliseconds} ms.");
                tokens = lexer.Tokens;
            }
            // tokens.ForEach(Console.WriteLine);
            // watch.Restart();
            var parser = new Parser(tokens);
            List<Statement> statements = parser.Parse();
            // Console.WriteLine($"Parsing took {watch.ElapsedMilliseconds} ms.");
            // watch.Restart();
            if (Handler.HadError) return;
            var resolver = new Resolver(evaluator);
            resolver.Resolve(statements);
            // Console.WriteLine($"Resolution took {watch.ElapsedMilliseconds} ms.");
            // watch.Restart();
            if (Handler.HadError) return;
            // var p = new StmtPrinter();
            // Console.WriteLine(p.Print(statements));
            try
            {
                evaluator.Execute(statements);
            }
            catch (RuntimeException){}
            // Console.WriteLine($"Execution took {watch.ElapsedMilliseconds} ms.");
            // watch.Stop();
        }

        public void InterpretFile(string path)
        {
            
        }
    }
}   