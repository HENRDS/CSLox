using System;
using System.Linq;
using System.Collections.Generic;
namespace CSLox
{
    public class ErrorHandler
    {
        private static readonly ErrorHandler instance = new ErrorHandler();
        public static ErrorHandler Instance => instance;
        static readonly string[] names = {"Error", "Warning", "Info", "Debug"};
        private enum ReportType 
        {
            ERROR, WARNING, INFO, DEBUG
        }
        private ErrorHandler()
        {
            HadError= false;
        }

        public void Debug<T>(String message, Token token = null) => 
            Report<T>(ReportType.DEBUG, message, token);
        public void Info<T>(String message, Token token = null) => 
            Report<T>(ReportType.INFO, message, token);
        public void Warning<T>(String message, Token token = null) => 
            Report<T>(ReportType.WARNING, message, token);
        public void Error<T>(String message, Token token)
        {
            HadError = true;
            Report<T>(ReportType.ERROR, message, token);
        }
        public void Error<T>(String message, int line, int column)
        {
            HadError = true;
            Console.WriteLine($"{typeof(T).Name} Error[{line}, {column}]: {message}");
        }
        public bool HadError { get; set;}
        private void Report<T>(ReportType type, String message, Token token = null)
        {
            string tk = token == null ? "" : $"at '{token.Lexeme}' ";
            string pos = token == null ? "" : $"[{token.Line}, {token.Column}]";
            Console.WriteLine($"{typeof(T).Name} {names[(int)type]}{pos} {tk}: {message}");
        }
    }
}