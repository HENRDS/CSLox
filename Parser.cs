using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace CSLox
{

    public class Parser
    {
        #region Properties & Fields
        readonly List<Token> Tokens;

        int Current = 0;
        #endregion
        
        #region Utility
        private bool IsAtEnd => (Current >= Tokens.Count) || (Peek().Type == TokenType.Eof);

        private Token Peek(int offset = 0) => Tokens[Current + offset];
        private Token PeekBack() => Tokens[Current-1];

        private Token Next() => Tokens[Current++];

        private ParseException Error(String message, int? line = null)
        {
            Lox.Handler.Error<Parser>(message, Peek());
            return message == null ? new ParseException(Peek()) : new ParseException(Peek(), message);
        }
        private Token Consume(TokenType token, string errorMessage) 
        {
            if (Check(token)) return Next();
            throw Error(errorMessage);
        }

        private bool Check(TokenType tk) => !IsAtEnd && Peek().Type == tk; 
        private bool Match(params TokenType[] tokens)
        {
            if (tokens.Aggregate(false, (acc, tk) => acc || Check(tk)))
            {
                Next();
                return true;
            }
            return false;
        }

        private void Synchronize()
        {
            Next();

            while(!IsAtEnd) {
                if (Peek(-1).Type == TokenType.Semicolon) return;
                switch (Peek().Type)
                {
                    case TokenType.Class:
                    case TokenType.Fun:
                    case TokenType.Var:
                    case TokenType.For:
                    case TokenType.If:
                    case TokenType.While:
                    case TokenType.Print:
                    case TokenType.Return:
                        return;
                }
                Next();
            }
        } 
        #endregion

        public Parser(List<Token> tokens) 
        {
            this.Tokens = tokens;
        }


        public List<Statement> Parse()
        {
            var statements = new List<Statement>();
            try
            {    
                while(!IsAtEnd) {
                    statements.Add(Declaration());
                }
            }
            catch (ParseException){}
            return statements;
        }


        private Statement Declaration()
        {
            try 
            {
                if (Match(TokenType.Var)) return VarDeclaration();
                if (Match(TokenType.Const)) return ConstDeclaration();
                return Statement();
            }
            catch (ParseException)
            {
                Synchronize();
                return null;
            }
        }

        private Statement Statement()
        {
            if (Match(TokenType.Class)) return Class();
            if (Match(TokenType.Print)) return Print();
            if (Match(TokenType.If)) return If();
            if (Match(TokenType.While)) return While();
            if (Match(TokenType.Fun)) return Function("function");
            if (Match(TokenType.For)) return For();
            if (Match(TokenType.LBrace)) return Block();
            if (Match(TokenType.Return)) return Return();

            return ExpressionStatement();
        }

        private Statement Class()
        {
            Token name = Consume(TokenType.Identifier, "Expected name for class.");
            Consume(TokenType.LBrace, "Expected { before class body");
            var methods = new List<Statement.Function>();
            while(!Check(TokenType.RBrace) && !IsAtEnd)
            {
                methods.Add((Statement.Function)Function("method"));
            }
            Consume(TokenType.RBrace, "Expected } after class body");
            return new Statement.Class(name, methods);
        }


        private Statement Return()
        {
            Token keyword = Peek(-1);
            Expression value = null;
            if (!Check(TokenType.Semicolon))
                value = Expression();
            Consume(TokenType.Semicolon, "Expected ';' after return statement.");
            return new Statement.Return(keyword, value);
        }

        private List<Token> Parameters(string kind)
        {
            Consume(TokenType.LParen, $"Expected '(' before {kind} parameters.");
            var parameters = new List<Token>();
            if (!Check(TokenType.RParen))
            {
                do
                {
                    parameters.Add(Consume(TokenType.Identifier, "Expected parameter name"));
                } while (Match(TokenType.Comma));
            }
            Consume(TokenType.RParen, $"Expected ')' after {kind} parameters.");
            return parameters;
        }

        private List<Statement> Body(string kind)
        {
            Consume(TokenType.LBrace, $"Expected '{{' before {kind} body.");
            return ((Statement.Block)Block()).Statements;
        }

        private Statement Function(string kind)
        {
            Token name = Consume(TokenType.Identifier, $"Expected {kind} name.");
            List<Token> parameters = Parameters(kind);
            List<Statement> body = Body(kind);
            return new Statement.Function(name, parameters, body);
        }

        private Statement Block()
        {
            var statements = new List<Statement>();
            while (!Check(TokenType.RBrace) && !IsAtEnd)
                statements.Add(Declaration());
            Consume(TokenType.RBrace, "Expected '}' after block.");
            return new Statement.Block(statements);
        }

        private Statement VarDeclaration()
        {
            Token name = Consume(TokenType.Identifier, "Expected variable name.");
            Expression initializer = null;

            if (Match(TokenType.Equals))
                initializer = Expression();
            Consume(TokenType.Semicolon, "Expected ';' after variable declaration");
            return new Statement.Var(name, initializer);
        }

        private Statement ConstDeclaration()
        {
            Token name = Consume(TokenType.Identifier, "Expected constant name.");
            Consume(TokenType.Equals, "Expected '=', constant must have a value");
            Expression value = Expression();
            Consume(TokenType.Semicolon, "Expected ';' after constant declaration");
            return new Statement.Const(name, value);
        }


        private Statement If()
        {
            Consume(TokenType.LParen, "Expected '(' after if.");
            Expression condition = Expression();
            Consume(TokenType.RParen, "Expected ')' after if.");
            Statement thenBranch = Statement();
            Statement elseBranch = null;
            if (Match(TokenType.Else))
                elseBranch = Statement();
            return new Statement.If(condition, thenBranch, elseBranch);
            
        }

        private Statement For()
        {
            Consume(TokenType.LParen, "Expected '(' after for.");
            Statement initializer;
            if (Match(TokenType.Semicolon)) initializer = null;
            else if (Match(TokenType.Var)) initializer = VarDeclaration();
            else initializer = ExpressionStatement();

            Expression condition = null;
            if (!Check(TokenType.Semicolon)) condition = Expression();
            Consume(TokenType.Semicolon, "Expect ';' after loop condition.");
            
            Expression increment = null;
            if (!Check(TokenType.RParen)) increment = Expression();
            Consume(TokenType.Semicolon, "Expect ')' after for clauses.");

            Statement body = Statement();
            
            if (increment == null) 
                body = new Statement.Block(new List<Statement>(){body, new Statement.Expression(increment)});
            body = new Statement.While(condition ?? new Expression.Literal(true), body);
            if (initializer == null)
                body = new Statement.Block(new List<Statement>() { initializer , body});
            return body;
        }
        private Statement While()
        {
            Consume(TokenType.LParen, "Expected '(' after while.");
            Expression condition = Expression();
            Consume(TokenType.RParen, "Expected ')' after while.");
            Statement body = Statement();
            return new Statement.While(condition, body);

        }
        private Statement Print()
        {
            Expression value = Expression();
            Consume(TokenType.Semicolon, "Expected ';' after value");
            return new Statement.Print(value);
        }

        private Statement ExpressionStatement()
        {
            Expression value = Expression();
            Consume(TokenType.Semicolon, "Expected ';' after expression");
            //if (Peek().Type == TokenType.SCOLON) Next();
            return new Statement.Expression(value);
        }

        private Expression LeftAssocBinRule(Func<Expression> funct, params TokenType[] matches)
            => LeftAssocRule(funct, (l, op, r) => new Expression.Binary(l, op, r), matches);

        private Expression LeftAssocLogRule(Func<Expression> funct, params TokenType[] matches)
            => LeftAssocRule(funct, (l, op, r) => new Expression.Logic(l, op, r), matches);


        private Expression LeftAssocRule(Func<Expression> funct, 
            Func<Expression, Token, Expression, Expression> accumulator, params TokenType[] matches)
        {
            Expression expr = funct();

            while (Match(matches))
            {
                Token oper = PeekBack();
                Expression right = funct();
                expr = accumulator(expr, oper, right);
            }
            return expr;
        }
        

        private Expression Expression() => Comma();


        // comma -> Conditional(, Conditional)*
        private Expression Comma() 
        {
            var xps = new List<Expression>();
            do
            {
                xps.Add(Assignment());
            } 
            while (Match(TokenType.Comma));
            return xps.Count == 1 ? xps[0] : new Expression.Comma(xps);
        }

        // conditional -> equality ? conditional : conditional;
        private Expression Conditional() 
        {
            Expression expr = LogicOr();
            if(Match(TokenType.Question)) 
            {
                Expression thenExpr = Conditional();
                if (Match(TokenType.Colon))
                {
                    Expression elseExpr = Conditional();
                    expr = new Expression.Conditional(expr, thenExpr, elseExpr);
                }
                else
                {
                    throw Error("Malformed expression");
                }
            }
            return expr;
        }

        // assignment -> variable = assignment
        private Expression Assignment()
        {
            Expression expr = Conditional();
            Expression value = null;
            if (!Match(TokenType.Equals))
                return expr;
            
            
            Token eq = Peek(-1);
            switch (expr)
            {
                case Expression.Variable v:
                    value = Assignment();
                    return new Expression.Assignment(v.Name, value);
                case Expression.Get get:
                    value = Assignment();
                    return new Expression.Set(get.Object, get.Name, value);
                default:
                    Error("Invalid assignment target.");
                    return expr;
                }
            
        }
        
        // Or -> And ("or" And)
        private Expression LogicOr() => LeftAssocLogRule(LogicAnd, TokenType.PipePipe);

        // And -> Equality ("and" equality)
        private Expression LogicAnd() => LeftAssocLogRule(Equality, TokenType.AmpersandAmpersand);

        // equality → comparison ( ( "!=" | "==" ) comparison )* ; // 'a when 'a: comparison;  lol
        private Expression Equality() => 
            LeftAssocBinRule(Comparison, TokenType.EqualsEquals, TokenType.BangEquals);

        // comparison → xor ( ( ">" | ">=" | "<" | "<=" | "|" | "&") xor )* ;
        private Expression Comparison() => 
            LeftAssocBinRule(Or, TokenType.Greater, TokenType.GreaterEquals, TokenType.Less, TokenType.LessEquals);

        // xor -> addition ( "^" addition)*
        private Expression Or() => LeftAssocBinRule(And, TokenType.Pipe);
        private Expression And() => LeftAssocBinRule(Xor, TokenType.Ampersand);
        private Expression Xor() => LeftAssocBinRule(Addition, TokenType.Caret);

        // addition → multiplication ( ( "-" | "+" ) multiplication )* ;
        private Expression Addition() =>
            LeftAssocBinRule(Multiplication, TokenType.Plus, TokenType.Minus);

        // multiplication → power ( ( "/" | "*") power )* ;
        private Expression Multiplication() =>
            LeftAssocBinRule(Power, TokenType.Star, TokenType.Slash);

        // power -> unary ("^" unary | composite) 
        private Expression Power() =>
            LeftAssocBinRule(Unary, TokenType.StarStar, TokenType.Slash);


        // unary → ( "!" | "-" | "~") unary | primary ; 
        private Expression Unary() 
        {
            if (Match(TokenType.Minus, TokenType.Bang, TokenType.Tilde))
            {
                Token oper = PeekBack();
                Expression right= Unary();
                return new Expression.Unary(oper, right);
            }
            return Call();
        }

        private Expression Call()
        {
            Expression expression = Primary();

            while (true) 
            {
                if (Match(TokenType.LParen)) expression = FinishCall(expression);
                else if (Match(TokenType.Dot))
                {
                    Token name = Consume(TokenType.Identifier, "Expect property name after '.'.");
                    expression = new Expression.Get(expression, name);
                }
                else break;
            } 
            return expression;
        }

        private Expression FinishCall(Expression callee)
        {
            var args = new List<Expression>();
            if (!Check(TokenType.RParen))
            {
                do
                {
                    args.Add(Expression());
                }
                while(Match(TokenType.Comma));
            }

            Token paren = Consume(TokenType.RParen, "Expect ')' after arguments.");

            return new Expression.Call(callee, paren, args);
        }

        private List<Token> LamdaParams()
        {
            bool hasParens = false;
            if (Match(TokenType.MinusGreater)) throw Error("For parameterless lambda expression use \\()->expr.");
            var parameters = new List<Token>();
            hasParens = Match(TokenType.LParen);
            do 
            {
                parameters.Add(Consume(TokenType.Identifier, "Expected parameter name."));
            } while(Match(TokenType.Comma)); // add support to whitespaces
            if (hasParens) Consume(TokenType.RParen, "Mismatch of parenthesis.");
            return parameters;
        }

        private Expression Lambda()
        {
            Token keyword = Peek(-1);
            List<Token> parameters = LamdaParams();
            Token arrow = Consume(TokenType.MinusGreater, "Expected '->' after lambda parameters.");
            List<Statement> body;
            if (Match(TokenType.LBrace))  
            {
                body = Body("lamda");
            }
            else 
            {
                body = new List<Statement>();
                body.Add(new Statement.Return(arrow, Expression()));
            }
            return new Expression.Lambda(keyword, parameters, body);
        }

        // primary → NUMBER | STRING | "false" | "true" | "nil" | "(" expression ")" ;
        private Expression Primary()
        {
            if (Match(TokenType.True)) return new Expression.Literal(true);
            if (Match(TokenType.False)) return new Expression.Literal(false);
            if (Match(TokenType.Nil)) return new Expression.Literal(null);
            if (Match(TokenType.Num, TokenType.Str, TokenType.Int))
                return new Expression.Literal(Peek(-1).Literal);
            if (Match(TokenType.Identifier)) 
            {
                // if (Match(TokenType.MinusMinus, TokenType.Plus))
                return new Expression.Variable(Peek(-1));
            }
                
            if (Match(TokenType.LParen)) 
            {  
                Expression expr = Expression();
                Consume(TokenType.RParen, "Expected ')' after expression.");
                return new Expression.Grouping(expr);

            } 
            if (Match(TokenType.Backslash)) return Lambda();
            if (Match(TokenType.This)) return new Expression.This(Peek(-1));

            throw Error($"Expected expression.");
        }

        
    }
}