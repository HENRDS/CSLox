namespace CSLox
{
    public enum TokenType
    {
        LParen, RParen, LBrace, RBrace, Comma, Dot, Minus, Plus, Semicolon, Slash, Star, PlusPlus, MinusMinus,
        Question, Colon, Ampersand, Pipe, AmpersandAmpersand, PipePipe, Caret, Tilde, StarStar, Backslash, MinusGreater,

        Bang, BangEquals, Equals, EqualsEquals, Greater, GreaterEquals, Less, LessEquals,

        Identifier, Str, Num, Int, WildCard,

        Class, Else, False, Fun, For, If, Nil, Print, Return, Super, This, True, Var, While, Const,

        Eof
    }
    public sealed class Token
    {
        public readonly TokenType Type;
        public readonly string Lexeme;
        public readonly object Literal;
        public readonly int Line;
        public readonly int Column;
        
        public Token(TokenType type, string lexeme, object literal, int line, int column) 
        {
            this.Type = type;
            this.Lexeme = lexeme;
            this.Literal = literal;
            this.Line = line;
            this.Column = column;
        }

        public override string ToString() 
        {
            string position = $" at ({this.Line},{this.Column})";
            switch (this.Type)
            {   
                case TokenType.Int:
                case TokenType.Num:
                case TokenType.Str: return $"{this.Type}({this.Lexeme})=\"{this.Literal}\"{position}";
                case TokenType.Nil: return $"Nil{position}";
                default: return $"{this.Type}({this.Lexeme}){position}";
            }
        }

    }
}