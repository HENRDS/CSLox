using System;
using System.Linq;
using System.Collections.Generic;

namespace CSLox
{
    public class Evaluator : IExpressionVisitor<object>, IStatementVisitor<object>
    {
        private readonly Dictionary<Expression, int> Locals = new Dictionary<Expression, int>();
        public readonly Environment Globals = new Environment();
        public Environment environment {get; private set;}

        public Evaluator()
        {
            this.environment = Globals;
            Globals.DefineSafe("clock", new LoxNative(0, (ev, args) => (double)DateTime.Now.TimeOfDay.Milliseconds));
        }

        public void Execute(Statement statement)
        {
            statement.Accept(this);
        }        

        public void Execute(List<Statement> statements)
        {
            // var printer = new StmtPrinter();
            foreach (var st in statements) 
            {
                // Console.WriteLine(printer.Print(st));
                Execute(st);
            }
        }
        public object Evaluate(Expression expression)
        {
            return expression.Accept(this);
        }

        public void Resolve(Expression expression, int depth) => this.Locals.Add(expression, depth);

        private object LookupVariable(Token name, Expression expression)
        {
            int distance;
            if (Locals.TryGetValue(expression, out distance))
                return environment[name, distance];
            return Globals[name];
        }
        private RuntimeException Error(string message, Token @operator)
        {
            Lox.Handler.Error<Evaluator>(message, @operator);
            return new RuntimeException(@operator, message);
        }
        private void CheckNumOperads(Token @operator, params object[] operands)
        {
            if (!operands.Aggregate(true, (acc, op)=> acc && op is double))
                throw Error("Operand(s) must be number(s).", @operator);
        }

        public void ExecuteBlock(List<Statement> block, Environment newEnv = null)
        {
            Environment prev = this.environment;
            try
            {
                this.environment = newEnv ?? new Environment(prev);
                Execute(block);
            }
            finally
            {
                this.environment = prev;
            }
        }

        private bool AreEqual(object a, object b, Token op)
        {
            if (a.GetType() != b.GetType())
                throw Error("Operators must have the same type", op);
            if(a == null && b == null) return true;
            if(a == null) return false;
            return a.Equals(b);
        }

        private bool AsBool(object obj)
        {
            switch (obj)
            {
                case null: return false;
                case double d: return d != 0;
                case bool b: return b;      
                default: return true;
            }
        }

        public string Stringify(object obj)
        {
            return obj?.ToString() ?? "nil";
        }

        object IExpressionVisitor<object>.visitExprAssignment(Expression.Assignment assignment)
        {
            object value = Evaluate(assignment.Value);
            int distance;
            if (Locals.TryGetValue(assignment.Value, out distance))
                environment[assignment.Name, distance] = value;
            else
                Globals[assignment.Name] = value;
            return value;
        }

        object IExpressionVisitor<object>.visitExprGrouping(Expression.Grouping grouping)
        {
            return Evaluate(grouping.Expr);
        }

        object IExpressionVisitor<object>.visitExprConditional(Expression.Conditional conditional)
        {

            object condition = Evaluate(conditional.Condition);
            object @then = Evaluate(conditional.ThenExpr);
            object @else = Evaluate(conditional.ElseExpr);
            // whatever man
            // if (@then.GetType() != @else.GetType()) 
            // throw Error("Branches of conditional don't have the same type", null);
            // kind of an inception lol
            return AsBool(condition) ? @then : @else;
        }

        object StrOperations(object left, Token op, object right)
        {
            string a = left.ToString(), b = right.ToString();
            if (op.Type == TokenType.Plus) return a + b;
            else throw Error("Invalid operation for strings.", op);
        }
        object IntOperations(int left, Token op, int right)
        {
            switch (op.Type)
            {
                case TokenType.Plus:          return left + right;
                case TokenType.Minus:         return left - right;
                case TokenType.Star:          return left * right;
                case TokenType.Slash:
                    if (right == 0) throw Error("Division by zero.", op);         
                    return left / right;
                case TokenType.StarStar:      return Math.Pow(left, right);
                case TokenType.Ampersand:     return left & right;
                case TokenType.Pipe:          return left | right;
                case TokenType.Caret:         return left ^ right;
                case TokenType.Greater:       return left > right;
                case TokenType.GreaterEquals: return left >= right;
                case TokenType.Less:          return left < right;
                case TokenType.LessEquals:    return left <= right;
                default: throw Error("Invalid operation for int.", op);
            }
        }
        object DoubleOperations(double left, Token op, double right)
        {
            switch (op.Type)
            {
                case TokenType.Plus: return left + right;
                case TokenType.Minus: return left - right;
                case TokenType.Star: return left * right;
                case TokenType.Slash:
                    if (right == 0) throw Error("Division by zero.", op);
                    return left / right;
                case TokenType.StarStar: return Math.Pow(left, right);
                case TokenType.Greater: return left > right;
                case TokenType.GreaterEquals: return left >= right;
                case TokenType.Less: return left < right;
                case TokenType.LessEquals: return left <= right;
                default: throw Error("Invalid operation for double.", op);
            }
        }
        object IExpressionVisitor<object>.visitExprBinary(Expression.Binary binary)
        {
            object left = Evaluate(binary.Left);
            object right = Evaluate(binary.Right);
            Token op = binary.Operator;
            if (op.Type == TokenType.EqualsEquals) return AreEqual(left, right, op);
            if (op.Type == TokenType.BangEquals) return !AreEqual(left, right, op);
            if (left is string || right is string) return StrOperations(left, op, right);
            if (left is double && right is int) return DoubleOperations((double)left, op, (double)((int)right));
            if (left is int && right is double) return DoubleOperations((double)((int)left), op, (double)right);
            if (left is double && right is double) return DoubleOperations((double)left, op, (double)right);
            if (left is int && right is int) return IntOperations((int)left, op, (int)right);
            throw Error($"Invalid operation for {left.GetType()} and {right.GetType()}", op);
        }

        object IExpressionVisitor<object>.visitExprUnary(Expression.Unary unary)
        {
            object right = Evaluate(unary.Right);
            switch (unary.Operator.Type)
            {
                case TokenType.Minus:
                    if (right is int) return -(int)right;
                    if (right is double) return -(double)right;
                    throw Error("Invalid operator.", unary.Operator);
                case TokenType.Tilde:
                    if (right is int) return ~((int)right);
                    throw Error("Invalid operator.", unary.Operator);
                case TokenType.Bang: 
                    return !AsBool(right);
            }
            return null;
        }

        object IExpressionVisitor<object>.visitExprVariable(Expression.Variable variable)
        {
            return environment[variable.Name];
        }

        object IExpressionVisitor<object>.visitExprLiteral(Expression.Literal literal)
        {
            return literal.Value;
        }

        object IStatementVisitor<object>.visitStmtBlock(Statement.Block block)
        {
            ExecuteBlock(block.Statements);
            return null;
        }

        object IStatementVisitor<object>.visitStmtExpression(Statement.Expression expression)
        {
            var v = Evaluate(expression.Expr);
            if (Environment.IsREPL && !(expression.Expr is Expression.Assignment))
                Console.WriteLine(v);
            return null;
        }

        object IStatementVisitor<object>.visitStmtPrint(Statement.Print print)
        {
            object val = Evaluate(print.Expr);
            Console.WriteLine(Stringify(val));
            return null;
        }

        object IStatementVisitor<object>.visitStmtIf(Statement.If @if)
        {
            if (AsBool(Evaluate(@if.Condition)))
                Execute(@if.ThenBranch);
            else if (@if.ElseBranch != null)
                Execute(@if.ElseBranch);
            return null;
        }

        object IStatementVisitor<object>.visitStmtVar(Statement.Var var)
        {
            object value = null;
            if (@var.Initializer != null)
                value = Evaluate(@var.Initializer);
            environment.Define(@var.Name, value);
            return null;
        }

        object IStatementVisitor<object>.visitStmtConst(Statement.Const @const)
        {
            object value = Evaluate(@const.Value);
            environment.DefineConst(@const.Name, value);
            return null;
        }

        object IExpressionVisitor<object>.visitExprLogic(Expression.Logic logic)
        {
            object left = Evaluate(logic.Left);
            return (logic.Operator.Type == TokenType.AmpersandAmpersand) ^ AsBool(left) ? left : Evaluate(logic.Right);
        }

        object IExpressionVisitor<object>.visitExprComma(Expression.Comma comma)
        {
            return comma.Expressions.Aggregate((object)null, (_, x) => Evaluate(x));
        }

        object IStatementVisitor<object>.visitStmtWhile(Statement.While @while)
        {
            while(AsBool(Evaluate(@while.Condition)))
                Execute(@while.Body);
            return null;
        }

        object IExpressionVisitor<object>.visitExprCall(Expression.Call call)
        {
            object callee = Evaluate(call.Callee);
            var args = call.Arguments.Select(Evaluate).ToList();
            ILoxCallable function = callee as ILoxCallable;
            // TODO : Implement partial application
            if (args.Count != function.Arity)
                throw Error($"Expected {function.Arity} arguments but got {args.Count}.", call.Paren);
            if (function == null)
                throw Error("Invalid invocation of non-callable expression.", call.Paren); 
            return function.Call(this, args);
        }

        object IStatementVisitor<object>.visitStmtFunction(Statement.Function function)
        {
            var fn = new LoxNamedFunction(function, this.environment, function.Name.Lexeme == "init");
            environment.Define(function.Name, fn);
            return null;
        }

        object IStatementVisitor<object>.visitStmtReturn(Statement.Return @return)
        {
            object value = null;
            if (@return.Value != null) value = Evaluate(@return.Value);
            throw new ReturnException(value);
        }

        object IExpressionVisitor<object>.visitExprLambda(Expression.Lambda lambda)
        {
            return new LoxLambda(lambda, this.environment);
        }

        object IStatementVisitor<object>.visitStmtClass(Statement.Class @class)
        {
            this.environment.Define(@class.Name, null);
            var methods = @class.Methods
                .Select((m) => new LoxNamedFunction(m, this.environment))
                .ToDictionary((m) => m.Name);
            
            var cls = new LoxClass(@class, methods);
            this.environment[@class.Name] = cls;
            

            throw new NotImplementedException();
        }

        object IExpressionVisitor<object>.visitExprGet(Expression.Get get)
        {
            object obj = this.Evaluate(get.Object);
            if (obj is LoxInstance)
            {
                return ((LoxInstance) obj)[get.Name];
            }
            throw Error("Only instances have properties", get.Name);
        }

        object IExpressionVisitor<object>.visitExprSet(Expression.Set set)
        {
            object obj = this.Evaluate(set.Object);
            if (obj is LoxInstance)
            {
                object value = Evaluate(set.Value);
                return ((LoxInstance)obj)[set.Name] = value;
            }
            throw Error("Only instances have properties", set.Name);
        }

        object IExpressionVisitor<object>.visitExprThis(Expression.This @this)
        {
            return LookupVariable(@this.Keyword, @this);
        }
    }
}