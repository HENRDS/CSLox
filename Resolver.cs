using System;
using System.Linq;
using System.Collections.Generic;

namespace CSLox
{
    public class Resolver : IStatementVisitor<object>, IExpressionVisitor<object>
    {
        private readonly Evaluator evaluator;

        private List<Dictionary<string, bool>> Scopes;
        public enum FunctionType {
            NONE, FUNCTION, LAMBDA, METHOD, INIT
        }
        public enum ClassType {
            NONE, CLASS
        }
        
        //break & continue
        private bool IsInsideLoop = false;
        private FunctionType CurrentFunction = FunctionType.NONE;
        private ClassType CurrentClass = ClassType.NONE;

        private bool isScopesEmpty => Scopes.Count == 0;

        public Resolver(Evaluator evaluator)
        {
            this.evaluator = evaluator;
            this.Scopes = new List<Dictionary<string, bool>>(); 
        }

        public void Resolve(List<Statement> statements) => statements.ForEach((Statement s) => this.Resolve(s));
        private void Resolve(Statement statement) => statement.Accept(this);
        private void Resolve(Expression expression) => expression.Accept(this);

        private void ResolveLocal(Expression expression, Token name)
        {
            for(var i = Scopes.Count - 1; i > 0; --i)
            {
                if(Scopes[i].ContainsKey(name.Lexeme))
                {
                    this.evaluator.Resolve(expression, Scopes.Count - 1 - i);
                    return;
                }
            }
        }

        private void ResolveFunction(Statement.Function function, FunctionType type)
        {
            FunctionType enclosing = CurrentFunction;
            CurrentFunction = type;
            BeginScope();
            foreach (var param in function.Parameters) {
                Declare(param); 
                Define(param);
            }
            Resolve(function.Body);
            EndScope();
            CurrentFunction = enclosing;
        }

        private void BeginScope() => Scopes.Insert(0, new Dictionary<string, bool>());
        private void EndScope() => Scopes.RemoveAt(0);

        private void Declare(Token name)
        {
            if (isScopesEmpty) return;
            Scopes.First()[name.Lexeme] = false;
        }
        private void Define(Token name)
        {
            if (isScopesEmpty) return;
            Scopes.First()[name.Lexeme] = true;
        }

        object IExpressionVisitor<object>.visitExprAssignment(Expression.Assignment assignment)
        {
            Resolve(assignment.Value);
            ResolveLocal(assignment, assignment.Name);
            return null;
        }

        object IExpressionVisitor<object>.visitExprBinary(Expression.Binary binary)
        {
            Resolve(binary.Left);
            Resolve(binary.Right);
            return null;
        }

        object IExpressionVisitor<object>.visitExprCall(Expression.Call call)
        {
            Resolve(call.Callee);
            call.Arguments.ForEach(this.Resolve);
            return null;
        }

        object IExpressionVisitor<object>.visitExprComma(Expression.Comma comma)
        {
            comma.Expressions.ForEach(this.Resolve);
            return null;
        }

        object IExpressionVisitor<object>.visitExprConditional(Expression.Conditional conditional)
        {
            Resolve(conditional.Condition);
            Resolve(conditional.ThenExpr);
            Resolve(conditional.ElseExpr);
            return null;
        }

        object IExpressionVisitor<object>.visitExprGrouping(Expression.Grouping grouping)
        {
            Resolve(grouping.Expr);
            return null;
        }

        object IExpressionVisitor<object>.visitExprLambda(Expression.Lambda lambda)
        {
            FunctionType enclosing = CurrentFunction;
            CurrentFunction = FunctionType.LAMBDA;
            BeginScope();
            // Console.WriteLine(Scopes.Count);
            foreach (var param in lambda.Parameters)
            {
                Declare(param);
                Define(param);
            }
            Resolve(lambda.Body);
            // Console.WriteLine(Scopes.Count);
            EndScope();
            CurrentFunction = enclosing;
            return null;
        }

        object IExpressionVisitor<object>.visitExprLiteral(Expression.Literal literal) => null;

        object IExpressionVisitor<object>.visitExprLogic(Expression.Logic logic)
        {
            Resolve(logic.Left);
            Resolve(logic.Right);
            return null;
        }

        object IExpressionVisitor<object>.visitExprUnary(Expression.Unary unary)
        {
            Resolve(unary.Right);
            return null;
        }

        object IExpressionVisitor<object>.visitExprVariable(Expression.Variable variable)
        {
            if (!isScopesEmpty && 
                Scopes.First().ContainsKey(variable.Name.Lexeme) &&
                !Scopes.First()[variable.Name.Lexeme])
            {
                Lox.Handler.Error<Resolver>("Cannot read local variable in its own initializer", variable.Name);
            }
            ResolveLocal(variable, variable.Name);
            return null;
        }

        object IStatementVisitor<object>.visitStmtBlock(Statement.Block block)
        {
            BeginScope();
            Resolve(block.Statements);
            EndScope();
            return null;
        }

        object IStatementVisitor<object>.visitStmtConst(Statement.Const @const)
        {
            Declare(@const.Name);
            Resolve(@const.Value);
            Define(@const.Name);
            return null;
        }

        object IStatementVisitor<object>.visitStmtExpression(Statement.Expression expression)
        {
            Resolve(expression.Expr);
            return null;
        }

        object IStatementVisitor<object>.visitStmtFunction(Statement.Function function)
        {
            Declare(function.Name);
            Define(function.Name);
            ResolveFunction(function, FunctionType.FUNCTION);
            return null;
        }

        object IStatementVisitor<object>.visitStmtIf(Statement.If @if)
        {
            Resolve(@if.Condition);
            Resolve(@if.ThenBranch);
            if (@if.ElseBranch != null) Resolve(@if.ElseBranch);
            return null;
        }

        object IStatementVisitor<object>.visitStmtPrint(Statement.Print print)
        {
            Resolve(print.Expr);
            return null;
        }

        object IStatementVisitor<object>.visitStmtReturn(Statement.Return @return)
        {
            if (CurrentFunction == FunctionType.NONE)
                Lox.Handler.Error<Resolver>("Cannot return from top-level code.", @return.Keyword);
            if (CurrentFunction == FunctionType.INIT)
                Lox.Handler.Error<Resolver>("Cannot return from initializer..", @return.Keyword);
            if (@return.Value != null)
                Resolve(@return.Value);
            return null;
        }

        object IStatementVisitor<object>.visitStmtVar(Statement.Var var)
        {
            Declare(var.Name);
            if (var.Initializer != null)
                Resolve(var.Initializer);
            Define(var.Name);
            return null;
        }

        object IStatementVisitor<object>.visitStmtWhile(Statement.While @while)
        {
            bool enclosing = IsInsideLoop;
            IsInsideLoop = true;
            Resolve(@while.Condition);
            Resolve(@while.Body);
            IsInsideLoop = enclosing;
            return null;
        }

        object IStatementVisitor<object>.visitStmtClass(Statement.Class @class)
        {
            Declare(@class.Name);
            Define(@class.Name);
            ClassType enclosingClass = this.CurrentClass;
            this.CurrentClass = ClassType.CLASS;
            try
            {
                BeginScope();
                Scopes.First().Add("this", true);
                Action<Statement.Function> resolver = 
                    (m) => ResolveFunction(m, m.Name.Lexeme == "init"? FunctionType.INIT : FunctionType.METHOD);
                @class.Methods.ForEach(resolver);
                EndScope();
            }
            finally
            {
                this.CurrentClass = enclosingClass;
            }
            return null;
        }

        object IExpressionVisitor<object>.visitExprGet(Expression.Get get)
        {
            Resolve(get.Object);
            return null;
        }

        object IExpressionVisitor<object>.visitExprSet(Expression.Set set)
        {
            Resolve(set.Value);
            Resolve(set.Object);
            return null;
        }

        object IExpressionVisitor<object>.visitExprThis(Expression.This @this)
        {
            if (this.CurrentClass != ClassType.CLASS)
                Lox.Handler.Error<Resolver>("Cannot use 'this' outside of a class.", @this.Keyword);
            else
                ResolveLocal(@this, @this.Keyword);
            return null;
        }
    }
}