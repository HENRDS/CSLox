using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace CSLox.Lexing
{
    internal struct SourcePosition
    {
        public int Line;
        public int Column;
    }
    internal sealed class SourceReader : IDisposable
    {
        const int DefaultChunkSize = 4096;
        public const char InvalidChar = '\0';
        private readonly StreamReader Reader;
        // private char[] Buffer;
        private string Buffer;
        public int Start { get; private set; }
        public int Line { get; private set; }
        private int Current;
        public int LineStart { get; private set; }
        public int Column => this.Start - this.LineStart + 1;
        public bool IsAtEnd => this.Current >= this.Buffer.Length;//this.Reader.EndOfStream;
        public void Sync() => this.Start = this.Current;
        
        public string Capture(ref SourcePosition pos)
        {
            pos.Line = this.Line;
            pos.Column = this.Column;
            var res = this.Buffer.Substring(this.Start, this.Current - this.Start);
            this.Start = this.Current;
            return res;
        }
        public char Next() => this.Buffer[this.Current++];


        private bool InBufferRange(int x) => (x >= 0) && (x < Buffer.Length);

        public SourceReader(string path)
        {   
            this.Reader = new StreamReader(path, Encoding.UTF8);
            this.Start = 0;
            this.Line = 1;
            this.LineStart = 0;
            this.Buffer = this.Reader.ReadToEnd();
            // this.Reader.ReadBlock(Buffer, 0, Buffer.Length);
        }
        public void Close()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        public char Peek(int delta = 0)
        {
            if (this.IsAtEnd) return InvalidChar;
            int position = this.Current + delta;
            if (!InBufferRange(position))
                return InvalidChar;
                // ReadChunk();
            return this.Buffer[position];
        }
        public bool Match(char expected)
        {
            if (!IsAtEnd && this.Peek() == expected)
            {
                Current++;
                return true;
            }
            return false;
        }

        public void NewLine()
        {
            this.Line++;
            this.LineStart = this.Current;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }
                this.Reader.Close();
                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~SourceReader() 
        {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
          Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}