using System.IO;

namespace CSLox.Lexing
{
    internal sealed class SourceProvider
    {
        const uint DefaultChunkSize = (1 << 13); // 4KB
        private readonly FileStream Stream;
        private readonly uint ChunkSize;
        public SourceProvider(FileStream stream, uint chunkSize = DefaultChunkSize)
        {
            this.Stream = stream;
            this.ChunkSize = chunkSize;
        }

        // public bool ReadChunk(ref char[] buffer)
        // {
            
        // }

    }
}