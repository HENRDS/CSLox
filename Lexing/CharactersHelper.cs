namespace CSLox.Lexing
{
    internal static class CharactersHelper
    {
        public static bool IsIdentifierFirstChar(char c) => 
            (c >= 'A' && c <= 'Z') ||
            (c >= 'a' && c <= 'z') ||
            (c == '_') || (c == '\'');

        public static bool IsLineTerminator(char c) => 
            c == '\r'     || 
            c == '\n'     ||
            c == '\u0085' || 
            c == '\u2028' || 
            c == '\u2029';

        public static bool IsDecimalDig(char c) => c >= '0' && c <= '9';
        public static bool IsIdentifierChar(char c) => IsDecimalDig(c) || IsIdentifierFirstChar(c);
    }
}