using System;

namespace CSLox
{
    [System.Serializable]
    public class ReturnException : System.Exception
    {
        public readonly object Value;
        public ReturnException(object value) : base(null)
        {
            this.Value = value;
        }
        public ReturnException(object value, System.Exception inner) : base(null, inner) 
        { 
            this.Value = value;
        }
        protected ReturnException(
            object value,
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) 
            {
                this.Value = value;
            }
    }

    [System.Serializable]
    public class RuntimeException : System.Exception
    {
        public Token Token { get; }
        public RuntimeException(Token token, string message) : base(message) { this.Token = token; }
        public RuntimeException(Token token, string message, System.Exception inner) : base(message, inner) { this.Token = token; }
        protected RuntimeException(
            Token token,
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { this.Token = token; }
    }
    
    [System.Serializable]
    public class ParseException : System.Exception
    {
        public Token Token;
        public ParseException(Token token) : this(token, "Invalid token.") { }
        public ParseException(Token token, string message) : base(message) { this.Token = token; }
        public ParseException(Token token, string message, System.Exception inner) : base(message, inner) { this.Token = token; }
        protected ParseException(
            Token token,
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { this.Token = token; }
    }
}